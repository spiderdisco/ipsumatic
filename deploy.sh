#!/usr/bin/env bash
set -e

###############################################################################
# Deployment script to be run on server.
# - Generates & applies Nginx configuration.
# - Runs certbot.
###############################################################################


###############################################################################
# Args
###############################################################################
DOMAIN=$1


###############################################################################
# Generate Nginx config.
###############################################################################
echo " - Generating Nginx config..."
cat > /etc/nginx/sites-available/$DOMAIN <<EOF
server {
  listen 80;
  server_name $DOMAIN;

  error_log /srv/$DOMAIN/logs/nginx-error.log;
  access_log /srv/$DOMAIN/logs/nginx-access.log;

  location / {
    root /srv/$DOMAIN/;
    index index.html;
  }
}
EOF


###############################################################################
# Configure Nginx
###############################################################################
echo " - Configuring Nginx..."
rm -f /etc/nginx/sites-enabled/$DOMAIN
ln -s /etc/nginx/sites-available/$DOMAIN /etc/nginx/sites-enabled/$DOMAIN
mkdir -p /srv/$DOMAIN/logs/

echo " - Testing Nginx configuration..."
nginx -t

echo " - Restarting Nginx..."
systemctl restart nginx


###############################################################################
# Run CertBot
###############################################################################
echo " - Running CertBot..."
certbot --nginx --redirect --expand --non-interactive --agree-tos -m admin@spiderdis.co -d $DOMAIN


###############################################################################
# Success!
###############################################################################
echo -e "*** Success! ***"
echo "Deployed $DOMAIN"
