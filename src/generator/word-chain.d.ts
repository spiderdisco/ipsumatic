export type WordChain = {
	/** 2 stage Markov chain. */
	words: Map<string, Map<string, string[]>>;

	/** Entry point words. */
	entry: string[];
};