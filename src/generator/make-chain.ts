import { WordChain } from "./word-chain";

export const makeChain = (tokens: string[]): WordChain => {
	const chain: WordChain = {
		words: new Map<string, Map<string, string[]>>(),
		entry: []
	};

	let wordA: string | null = null;
	let wordB: string | null = null;

	tokens.forEach(token => {
		if(!wordA) {
			wordA = token;
			chain.entry.push(token);
			return;
		}

		if(!wordB) {
			wordB = token;
			return;
		}

		let map = chain.words.get(wordA);
		if(map === undefined) {
			map = new Map<string, string[]>();
			chain.words.set(wordA, map);
		}

		const arr = map.get(wordB) || [];
		arr.push(token);
		map.set(wordB, arr);

		if(isTerminalToken(token)) {
			wordA = null;
			wordB = null;
		} else {
			wordA = wordB;
			wordB = token;
		}
	});

	return chain;
};

export const isTerminalToken = (token: string): boolean => {
	if(!token.length) {
		return false;
	}

	switch(token.charAt(token.length - 1)) {
		case '.':
		case '!':
		case '?':
			return true;
		default:
			return false;
	}
};