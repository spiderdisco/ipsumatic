import { WordChain } from "./word-chain";

export type GeneratorParams = {
	length: number;
};

export const generateChain = (chain: WordChain, params: GeneratorParams): string => {
	if(chain.entry.length === 0) {
		return '';
	}
	
	let wordA: string | null = null;
	let wordB: string | null = null;

	const words: string[] = [];

	while(words.length < params.length) {
		const terminate = () => {
			wordA = null;
			wordB = null;
		};

		if(!wordA) {
			wordA = randArr(chain.entry);
			words.push(wordA);
		}

		if(!wordB) {
			const map = chain.words.get(wordA);
			if(!map) {
				terminate();
				continue;
			}

			wordB = randMapKey(map);
			words.push(wordB);
		}

		const map = chain.words.get(wordA);
		if(!map) {
			terminate();
			continue;
		}

		const arr = map.get(wordB);
		if(!arr) {
			terminate();
			continue;
		}

		const wordC = randArr(arr);
		words.push(wordC);

		wordA = wordB;
		wordB = wordC;
	}

	return words.join(' ');
};

const randIndex = (length: number): number => Math.floor(Math.random() * length);

export const randArr = <T> (arr: T[]): T => {
	if(arr.length === 0) {
		throw new Error("randMap(): arr was empty.");
	}

	if(arr.length === 1) {
		return arr[0];
	}

	return arr[randIndex(arr.length)];
};

const randMapKey = <K, V> (map: Map<K, V>): K => {
	if(map.size == 0) {
		throw new Error("randMap(): map was empty.");
	}
	
	return [...map.keys()][randIndex(map.size)];
};