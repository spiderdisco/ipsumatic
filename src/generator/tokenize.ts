export const tokenize = (src: string): string[] => {
	const tokens: string[] = [];

	let token ='';
	for(let i=0; i<src.length; ++i) {
		const char = src.charAt(i);



		switch(char) {
			case ' ':
				// End of token.
				tokens.push(token);
				token = '';
				break;

			case '{':
			case '}':
			case '(':
			case ')':
			case '[':
			case ']':
			case '"':
			case `'`:
				// Undesirable punctuation, skip it.
				break;

			default:
				// Append character to current token.
				token = token + char;
				break;
		}
	}

	if(token.length) {
		tokens.push(token);
	}

	return tokens;
};