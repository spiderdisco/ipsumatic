import { WordChain } from "./word-chain";
import { makeChain } from "./make-chain";
import { tokenize } from "./tokenize";

export const parseChain = (source: string): WordChain => {
	return makeChain(tokenize(source));
};