import { WordChain } from "../generator/word-chain";
import { parseChain } from "../generator/parse";
import { generateChain } from "../generator/generate";

export namespace GeneratorService {

	///////////////////////////////////////////////////////////////////////////////
	// Source
	///////////////////////////////////////////////////////////////////////////////
	let source = '';
	let chain: WordChain;
	export const setSource = (newSource: string): void => {
		if(source === newSource) {
			return;
		}
		source = newSource;
		chain = parseChain(source);

		sourceChangeListeners.forEach(l => l(source));
		generate();

		console.log(`[GeneratorService] set source: ${source}`);
	};

	///////////////////////////////////////////////////////////////////////////////
	// Generate
	///////////////////////////////////////////////////////////////////////////////
	export const generate = (): void => {
		if(!chain) {
			return;
		}

		const result = generateChain(chain, { length: 150 });
		generateListeners.forEach(l => l(result));
	};


	///////////////////////////////////////////////////////////////////////////////
	// Listeners
	///////////////////////////////////////////////////////////////////////////////
	export type GeneratorListener = (result: string) => void;

	const generateListeners: GeneratorListener[] = [];
	export const onGenerate = (listener: GeneratorListener): void => {
		generateListeners.push(listener);
	};

	const sourceChangeListeners: GeneratorListener[] = [];
	export const onSourceChange = (listener: GeneratorListener): void => {
		console.log('[GeneratorService] add source listener: ' + listener);
		sourceChangeListeners.push(listener);
	};
}