import { h, render } from 'preact';
import { Root } from './components';
import { GeneratorService } from './services';

window.onload = () => {
	const root = document.getElementById('root');
	if(!root) {
		return;
	}

	render(<Root/>, root);

	setTimeout(() => {
		GeneratorService.setSource(initialSource);
		GeneratorService.generate();

		root.classList.remove('hide');
		root.classList.add('show');
	}, 100);
};

const initialSource = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
