import { h, FunctionComponent } from 'preact';
import classNames from 'classnames';
import { ClassProp } from '../util';
import { Output } from './output';
import { RegenerateButton } from './regenerate-button';
import { Source } from './source';

export const Generator: FunctionComponent<ClassProp> = p => {

	return (
		<main class={ classNames(p.class, 'generator-layout generator')}>
			<Source class="generator-layout__input"/>
			<menu class="generator-layout__controls">
				<RegenerateButton class="mrg-l-auto"/>
			</menu>
			<Output class="generator-layout__output"/>
		</main>
	);
};

