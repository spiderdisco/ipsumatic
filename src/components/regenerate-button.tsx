import { h, FunctionComponent } from 'preact';
import classNames from 'classnames';
import { ClassProp } from '../util';
import { useState, useEffect } from 'preact/hooks';
import { GeneratorService } from '../services';


export const RegenerateButton: FunctionComponent<ClassProp> = p => {
	const [ intervalHandle, setIntervalHandle ] = useState<number>(-1);

	const onMouseDown = () => {
		clearInterval(intervalHandle);
		const handle = setInterval(GeneratorService.generate, 50);
		setIntervalHandle(handle);
	};

	const onMouseUp = () => {
		clearInterval(intervalHandle);
	};

	return (
		<button
			class={classNames(p.class, 'btn btn--regenerate text-display text-large')}
			onMouseDown={onMouseDown}
			onMouseUp={onMouseUp}
			onMouseOut={onMouseUp}
		>
			REGENERATE!
		</button>
	);
};