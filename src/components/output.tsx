import { h, FunctionComponent } from 'preact';
import classNames from 'classnames';
import { ClassProp } from '../util/';
import { useState, useEffect } from 'preact/hooks';
import { GeneratorService } from '../services';


export const Output: FunctionComponent<ClassProp> = p => {
	const [ outputText, setOutputText ] = useState<string>('');

	useEffect(() => {
		GeneratorService.onGenerate(setOutputText);
	}, []);

	return (
		<article
			class={classNames(p.class, 'output text-large')}
		>
			{outputText}
		</article>
	);
};