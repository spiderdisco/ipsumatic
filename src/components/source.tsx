import { h, FunctionComponent } from 'preact';
import { useState, useRef, useEffect } from 'preact/hooks';
import classNames from 'classnames';
import { GeneratorService } from '../services';
import { ClassProp } from '../util';




export const Source: FunctionComponent<ClassProp> = p => {
	const [ sourceText, setSourceText ] = useState<string>('');

	const textarea = useRef<HTMLTextAreaElement>();

	const onInput = () => {
		if(!textarea.current) {
			return;
		}
		GeneratorService.setSource(textarea.current.value);
	};

	useEffect(() => {
		GeneratorService.onSourceChange(source => {
			setSourceText(source);
			console.log('listen ' + source);
		});
		setTimeout(() => {
			if(textarea.current) {
				textarea.current.focus();
			}
		});
	}, []);

	return (
		<textarea
			class={classNames(p.class, 'source text-small pad-md')}
			ref={textarea}
			onInput={onInput}
			spellcheck={false}
		>
			{sourceText}
		</textarea>
	);
};

