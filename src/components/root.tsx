import { h, FunctionComponent, } from 'preact';
import { Header } from './header';
import { Generator } from './generator';


export const Root: FunctionComponent = p => {

	return (
		<div class="root-layout">
			<Header class="root-layout__head"/>
			<Generator class="root-layout__body"/>
		</div>
	);
};