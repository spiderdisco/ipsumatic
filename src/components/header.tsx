import { h, FunctionComponent } from 'preact';
import { useState, useEffect } from 'preact/hooks';
import { randArr } from '../generator/generate';
import { ClassProp } from '../util';
import classNames from 'classnames';
import { GeneratorService } from '../services';

export const Header: FunctionComponent<ClassProp> = p => {
	const [ adjective, setAdjective ] = useState<string>('');

	const randomiseAdjective = () => {
		setAdjective(randArr(adjectives));
	};

	useEffect(() => {
		randomiseAdjective();
		GeneratorService.onGenerate(randomiseAdjective);
	}, []);

	console.log({});

	return (
		<h1 class={classNames(p.class, 'text-display text-header')}>
			<span class="text-large">
				the {adjective}&nbsp;
			</span>
			<span class="text-wide">
				IPSUMATIC!
			</span>
		</h1>
	);
};

const adjectives = [
	'incredible',
	'unbelievable',
	'impossible',
	'indescribable',
	'only',
	'usable',
	'lobstronomous',
	'subliminal',
	'website formerly known as',
	'markovian',
	'wonderful',
	'darling',
	'world renounced',
	'spectacular',
	'tubular',
	'mouthfeelable',
	's p a c i o u s',
	'pseudorandom',
	'inevitable',
	'extra',
	'first and last',
	'refined',
	'pangalactic',
	'unfathomable',
	'large hadron',
	'crapulous',
	'gastronomical',
	'invisible',
	'undefined',
	'definitive',
	'revenge of',
	'best of',
	'extraneous',
	'[object Object]',
	'neumorphic',
	'dark side of',
	'hand crafted',
	'never ending',
	'other',
	'proportional',
	'all singing all dancing',
	'infinite',
	'replicable',
	'smooooooooooth',
	'last of the',
	'whimsical',
	'autopoetic',
	'Giraffe and the Pelly and',
	'incorporeal',
	'cure for',
	'celebrated',
	'game of',
	'ineffable',
	'unironic',
	'glorious',
	'last of the summer',
	'last slice of',
	'wretched',
	'beloved',
	'ionizing',
	'stupendous',
	'essential',
	'dread pirate',
	'conformal',
	'superliminal',
	'hyper dimensional',
	'insatiable',
	'hound of the',
	'eternal wisdom of',
	'elder scrolls VI:',
	'conquest of',
	'oral history of',
	'the the',
	'theory of quantum',
	'dawn of the',
	'planet of the',
	'lorem',
	'mistaken idea of denouncing',
	'local',
	'one stop shop for all your',
	'one true',
	'incomprehensible',
	'formal',
	'deep lore of',
	'one with the',
	'journal of',
	'iconic',
	'dao of',
	'inevitable',
	'hard problem of',
	'cosmic microwave',
	'a e s t h e t i c',
	'completely bug free',
	'weakly interacting',
	'sporadic',
	'highly available',
	'outstanding',
	'harmless',
	'critically acclaimed',
	'stupendous',
	'indispensable',
	'lovely',
	'favourite',
	'unaccountable',
	'wonderful',
	'knowable',
	'independent',
	'nightmare before',	
];