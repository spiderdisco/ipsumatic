/* eslint-disable @typescript-eslint/no-non-null-assertion */

import 'mocha';
import { expect } from 'chai';

import { makeChain, isTerminalToken } from '../../../src/generator/make-chain';
import { WordChain } from '../../../src/generator/word-chain';


describe('makeChain()', () => {
	describe('makes chain for simple token sequence.', () => {
		const tokens = [
			'this', 'is', 'a', 'simple', 'sequence'
		];

		let chain: WordChain;
		before(() => chain = makeChain(tokens));

		it('finds entry', () => {
			expect(chain.entry, "collects entry")
				.to.deep.eq(['this']);
		});

		it('finds correct first words', () => {
			expect(chain.words.has('this'))
				.to.be.true;
			expect(chain.words.has('is'))
				.to.be.true;
			expect(chain.words.has('a'))
				.to.be.true;
			expect(chain.words.has('simple'))
				.to.be.false;
			expect(chain.words.has('sequence'))
				.to.be.false;
		});

		it('finds correct second words', () => {
			expect(chain.words.get('this')!.has('is'))
				.to.be.true;
				expect(chain.words.get('is')!.has('a'))
				.to.be.true;

		});

		it('finds correct word array', () => {
			expect(chain.words.get('this')!.get('is'))
				.to.deep.eq(['a']);
			expect(chain.words.get('is')!.get('a'))
				.to.deep.eq(['simple']);
			expect(chain.words.get('a')!.get('simple'))
				.to.deep.eq(['sequence']);
		});
	});


	describe('isTerminalToken()', () => {
		describe('returns false for non-terminal word', () => {
			['these', 'are', '6,', 'non-terminal', `word's`, '[sic]']
				.forEach(w => it(w, () => {
					expect(isTerminalToken(w))
						.to.be.false;
				}));
		});

		describe('returns true for non-terminal word', () => {
			['bla.', 'what?', 'wow!', 'etc...']
				.forEach(w => it(w, () => {
					expect(isTerminalToken(w))
						.to.be.true;
				}));
		});
	});
});