import 'mocha';
import { expect } from 'chai';

import { tokenize } from '../../../src/generator/tokenize';


describe('tokenize()', () => {
	it('returns empty array for empty string', () => {
		expect(tokenize(""))
			.to.be.empty;
	});

	it('returns array of correct length for simple string', () => {
		const src = 'five simple space separated words';
		expect(tokenize(src))
			.to.have.length(5);
	});

	it('keeps desired punctuation', () => {
		const src = 'a, sentence! with? some. punctuation...';
		expect(tokenize(src))
			.to.deep.eq([
				'a,',
				'sentence!',
				'with?',
				'some.',
				'punctuation...',
			]);
	});

	it('strips undesired punctuation', () => {
		const src = `(undesirable punctuation) {do not} [want] "to keep'`;
		expect(tokenize(src))
			.to.deep.eq([
				'undesirable',
				'punctuation',
				'do',
				'not',
				'want',
				'to',
				'keep',
			]);
	});
});